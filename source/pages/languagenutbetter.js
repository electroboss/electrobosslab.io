// get rid of delays
Util.setTimeoutLanguagenut = function (functionName, time) {
  if (window.languageNutTimeouts == null) {
    window.languageNutTimeouts = [];
  }
  var timeout = setTimeout(functionName, 0);
  window.languageNutTimeouts.push(timeout);
  return timeout;
};
Util.setIntervalLanguagenut = Util.setTimeoutLanguagenut;

// fridge magnets always go to the first selector
SentenceBuildingGameContainer.prototype.processDragEnd = function processDragEnd(magnet) {
  var lowestDistance = 9999;
  var closestSpace;
  for (var _i312 = this.destinationSpaces.length-1; _i312 >= 0; _i312--) {
    if (this.destinationSpaces[_i312].magnet == null) {
      closestSpace = this.destinationSpaces[_i312];
    }
    lowestDistance = 0;
    /*var distance = Util.distance(this.destinationSpaces[_i312].x, this.destinationSpaces[_i312].y, magnet.graphicObject.x, magnet.graphicObject.y);
    if (distance < lowestDistance) {
      lowestDistance = distance;
      closestSpace = this.destinationSpaces[_i312];
    }*/
  }
  if (lowestDistance < 60 && closestSpace.magnet == null) {
    closestSpace.setMagnet(magnet);
    magnet.setIsSet();
    window.renderer.refreshScreen();
  } else if (lowestDistance < 60 && closestSpace.magnet != null) {
    magnet.setPosition(this.originalSpaces[magnet.originalPosition].x - 9, this.originalSpaces[magnet.originalPosition].y - 6);
    window.renderer.refreshScreen();
  } else {
    magnet.setPosition(this.originalSpaces[magnet.originalPosition].x - 9, this.originalSpaces[magnet.originalPosition].y - 6);
  }
  var sentenceComplete = true;
  for (var _i313 = 0; _i313 < this.destinationSpaces.length; _i313++) {
    var space = this.destinationSpaces[_i313];
    if (space.magnet != null) {
      var _distance = Util.distance(space.x, space.y, space.magnet.graphicObject.x, space.magnet.graphicObject.y);
      if (_distance > 60) {
        space.magnet = undefined;
      }
    }
    if (space.magnet == null) {
      sentenceComplete = false;
    }
  }
  if (sentenceComplete) {
    this.enableSubmit(sentenceComplete);
  }
}
// fridge magnets respond to keypresses
SentenceBuildingGameContainer.prototype.build = function build() {
  this.canSubmit = false;
  this.offset = this.wordArray.length < 5 ? (5 - this.wordArray.length) * 110 : 0;
  this.buildQuestionBox();
  this.buildMagnets();
  this.buildTopLayer();
  this.buildBottomLayer();
  this.loadGraphicsObjects();
  document.addEventListener("keyup", (event) => {
    var num = Number(event.key);
    if (num == undefined) {
      return;
    }
      this.processDragEnd(this.magnets[num-1]);
    }
  );
}

// word pop bubbles freeze
WordPopBubble.prototype.build = function build() {
  var _this548 = this;
  var textWidth = Util.getTextSize(this.text) * 1.3;
  var radius = Math.max(50, Math.min(100, textWidth));
  var sizeModifier = Math.max(0.5, Math.min(1, textWidth / 100));
  this.compObject = new CompositeGraphicsObject();
  this.compObject.setPosition(this.x, this.y);
  this.compObject.setIsPhysics(true);
  this.compObject.setInitialVelocity([(Math.random() - Math.random()) / 5,
  (Math.random() - Math.random()) / 5]);
  this.compObject.setRadius(radius);
  this.compObject.setPhysicsType('circle');
  this.compObject.setVoidRotate(true);
  this.compObject.setAlphaLocked(true);
  this.compObject.friction = 0;
  this.bubble = new ImageGraphicsObject();
  this.bubble.setImage('new/wordpop/smallBubble.png');
  this.bubble.setPosition(0, 0);
  this.bubble.setSizeModifier(sizeModifier);
  var text = new TextGraphicsObject();
  text.setPosition(radius - 4, radius - 3);
  text.setText(this.text);
  text.setStyle(this.bubbleStyle);
  text.setAlignment('vcenter');
  text.setWordwrap(true, 160);
  text.setMaxHeight(80);
  this.compObject.addChildren([this.bubble,
  text]);
  this.compObject.setClickEvent(function () {
    return _this548.parent.submitAnswer(_this548);
  });
  setTimeout(()=>{this.compObject.setIsPhysics(false)},200);
}
window.WordPopController.prototype.randomDelay = () => {0}

// multiple choice options are key addressable
MultipleChoiceController.prototype.buildGraphics = function buildGraphics() {
  this.vocabBox = new MultipleChoiceController.VocabBox(this);
  this.vocabBox.build();
  this.options = [
  ];
  for (var _i272 = 0; _i272 < this.numOptions; _i272++) {
    var option = new Option(_i272, this);
    option.build();
    this.options.push(option);
  }
  document.addEventListener("keyup", (event) => {
    var num = Number(event.key);
    if (num == undefined) {
      return;
    }
    this.options[[0,2,1,3][num-1]].clickFunction();
  });
}