var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
ctx.font = "0.7rem sans-serif";
function go(){
  ctx.fillStyle = "#000000";
  ctx.fillRect(0,0,1030,530);
  ctx.fillStyle = "#00ff00";
  var number = Number(document.getElementById("number").value);
  if (number > 0 & (number == Math.floor(number))){
    var steps = Array()
    while (number != 1){
      steps.push(number)
      if (number%2 == 1){number = 3*number+1;}
      else if (number%2 == 0){number = number/2;}
    }
    steps.push(number)
    for (var i = 0; i < steps.length; i++){
      ctx.fillRect(30+((1000-10)/steps.length)*i,(500-(((500-10)/Math.max.apply(null,steps))*steps[i]))-10,10,10)
      ctx.fillText(i+1,30+((1000-10)/steps.length)*i,510);
      ctx.fillText(steps[i],40+((1000-10)/steps.length)*i,(500-(((500-10)/Math.max.apply(null,steps))*steps[i]))+10);
    }
    for (var i = 0; i <= 10; i++){
      ctx.fillText(((Math.floor(Math.max.apply(null,steps)/10))*i).toString(), 0, 500-(((500-10)/10)*i))
    }
    document.getElementById("output").innerText = "Count: " + steps.length.toString()
  } else {
    document.getElementById("output").innerText = "That's not a natural number!";
  }
}
