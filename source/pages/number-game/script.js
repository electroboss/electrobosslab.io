var number = 0;
var buttons = Array();
var maxnum;
var delta;

var start = function(starter) {
  number = 0;
  maxnum = Number(document.getElementById("maxnum").value);
  delta = Number(document.getElementById("delta").value);
  if (starter == 0){
    move();
  }
  update();
}
var update = function() {
  // number
  document.getElementById("number").innerText = "Number: " + number.toString()
  // buttons
  button_container = document.getElementById("button-container");
  button_container.innerHTML = "";
  for (var i=0; i<delta; i++) {
    var button = document.createElement("button");
    button.innerText = (number+i+1).toString();
    button.onclick = new Function("button_click("+i.toString()+")");
    button_container.appendChild(button);
    buttons.push(button);
  }
}
var button_click = function(id){
  number += id+1;
  if (number >= maxnum){
    document.getElementById("number").innerText = "You lose!";
    return;
  }
  move();
}
var move = function(){
  if (number >= (maxnum-1)) {
    document.getElementById("number").innerText = "You win!";
    return;
  }
  var x = ((maxnum-1)%(delta+1)-(number%(delta+1)))%(delta+1);
  if (x<1 || x>delta) {
    while (x < 0) {
      x+=(delta+1);
    }

    if (x<1 || x>delta) {
      console.log("x was "+x.toString()+", so chose random number.");
      var x = Math.floor(Math.random()*delta)+1;
    }
  }
  number += x;
  update();
}
