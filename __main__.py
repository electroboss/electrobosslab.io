from jinja2 import Environment, PackageLoader, select_autoescape
import os,logging
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

env = Environment(
  loader=PackageLoader("source"),
  autoescape=select_autoescape()
)

logging.basicConfig(filename='log.log',level=logging.DEBUG)

logging.info("Starting")

os.system("rm -rf public")
for root, dirs, files in os.walk("source/pages"):
  os.mkdir("public/"+root[12:])
  filedata = load(open(root+"/index.yaml","r").read(),Loader)
  rendered_file = open("public/"+root[12:]+"/index.html","w+")
  template = env.get_template(filedata["template"])
  params = dict()
  for i,param in enumerate(filedata["params"]):
    outparam=str()
    if filedata["params"][param][:2] == "t:":
      outparam = filedata["params"][param][2:]
    elif filedata["params"][param][:2] == "f:":
      outparamf = open(root+"/"+filedata["params"][param][2:],"r")
      outparam = outparamf.read()
      outparamf.close()
    params[param] = outparam
  
  if "static" in filedata.keys():
    logging.debug("Found static files")
    for x in filedata["static"]:
      f = open("public/"+root[12:]+"/"+x,"wb+")
      f.write(open(root+"/"+x, "rb").read())
      f.close()
  
  logging.debug(root+"/index.yaml"+"\t"+str(filedata["params"])+str(params))
  rendered_file.write(template.render(params))
  rendered_file.close()
